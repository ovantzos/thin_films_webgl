function initWebGL(canvas) {
  gl = null;
  try {
    gl = canvas.getContext("webgl2", {'antialias':false});
  }
  catch(e) {
  }
  if (gl) {
    /*if (!gl.getExtension("OES_texture_float")) {
      console.log("no OES_texture_float!");
    }*/
    if (!gl.getExtension("EXT_color_buffer_float")) {
      console.error("no EXT_color_buffer_float!");
    }
    if (!gl.getExtension("OES_texture_float_linear")) {
      console.error("no OES_texture_float_linear!");
    }
    //gl.getExtension("OES_texture_half_float");
    //gl.getExtension("OES_texture_half_float_linear");
    gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
    gl.clearColor(0,0,0,0);
  } else {
    alert("Unable to initialize WebGL. Your browser may not support it.");
  } 
  return gl;
}

function createShader(gl, type, source) {
  var shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (success) {
    return shader;
  } else {
    console.error(gl.getShaderInfoLog(shader));
    gl.deleteShader(shader);
  }
}

function createProgram(gl, vertexShader, fragmentShader, attribs={}) {
  var program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  for (var attr in attribs) {
    gl.bindAttribLocation(program,attribs[attr],attr);
  }
  gl.linkProgram(program);
  var success = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (success) {
    return program;
  } else {
    console.error(gl.getProgramInfoLog(program));
    gl.deleteProgram(program);
  }
}

function createQuad(gl) {
  var attribs = {"a_position":0, "a_texCoord":1};
  //position
  var positionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
    -1, -1,
    1, 1,
    1, -1,
    -1, -1,
    -1, 1,
    1, 1
  ]), gl.STATIC_DRAW);
  gl.enableVertexAttribArray(attribs["a_position"]);
  gl.vertexAttribPointer(attribs["a_position"], 2, gl.FLOAT, false, 0, 0);
  //texture coordinates
  var texCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, texCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
      0, 0,
      1, 1,
      1, 0,
      0, 0,
      0, 1,
      1, 1
  ]), gl.STATIC_DRAW);
  gl.enableVertexAttribArray(attribs["a_texCoord"]);
  gl.vertexAttribPointer(attribs["a_texCoord"], 2, gl.FLOAT, false, 0, 0);
  return attribs;
}

function drawQuad(gl) {
  gl.drawArrays(gl.TRIANGLES, 0, 6);
}

function createTexture(gl,source,canvasResolution=false,repeat=false,mipmap=false) {
  var texture = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, texture);
  if (source instanceof Image) {
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, source);
  } else {
    var size = canvasResolution ? gl.canvas : gl.resolution;
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA32F, +size.width, +size.height, 0, gl.RGBA, gl.FLOAT, source);
  }
  texture.size = size;
  // Set the parameters so we can render any size image.
  if (mipmap) { 
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, repeat ? gl.REPEAT : gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, repeat ? gl.REPEAT : gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR);
  } else {
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, repeat ? gl.REPEAT : gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, repeat ? gl.REPEAT : gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
  }
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
  return texture;
}

function setTextures(gl,fbo,program,textures) {
  var tex_slot = gl.TEXTURE0;
  var uniform = 0;
  for (var attr in textures) {
    if (attr=="out") {
      if (textures[attr]=="canvas") {
        gl.uniform1i(gl.getUniformLocation(program,"u_flip"),true);
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
      } else {
        gl.uniform1i(gl.getUniformLocation(program,"u_flip"),false);
        gl.bindFramebuffer(gl.FRAMEBUFFER,fbo);
        if (Array.isArray(textures[attr])) {
          let colorAttachments = [];
          for (let texture of textures[attr]) {
              gl.viewport(0, 0, texture.size.width, texture.size.height);
              let colorAttachment = gl.COLOR_ATTACHMENT0 + colorAttachments.length;
              colorAttachments.push(colorAttachment);
              gl.framebufferTexture2D(gl.FRAMEBUFFER, colorAttachment, gl.TEXTURE_2D, texture, 0);
          }
          gl.drawBuffers(colorAttachments);
        } else {
          gl.viewport(0, 0, textures[attr].size.width, textures[attr].size.height);
          gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, textures[attr], 0);
        }
      }
    } else {
      gl.activeTexture(tex_slot);
      gl.bindTexture(gl.TEXTURE_2D,textures[attr]);
      gl.uniform1i(gl.getUniformLocation(program,attr),uniform);
      tex_slot += 1;
      uniform += 1;
    }
  }
}

function download(size) {
  //var w = gl.canvas.width;
  //var h = gl.canvas.height;
  var pixels = new Float32Array(size * size * 4);
  gl.readBuffer(gl.COLOR_ATTACHMENT0);
  gl.readPixels(0, 0, size, size, gl.RGBA, gl.FLOAT, pixels);
  var csv = 'data:text/csv;charset=utf-8';
  console.log(pixels);
  pixels.forEach(function(val,ix,A) {
    csv += ','+val;
  });
  var encodedUri = encodeURI(csv);
  window.open(encodedUri);
}